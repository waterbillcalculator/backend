const express = require('express');
const router = express.Router();

const userControllers = require('../controllers/userControllers')

router.post('/', userControllers.getUserDetails);
router.post('/addUser', userControllers.addUser);
router.post('/updateUser/:id', userControllers.updateUser);
router.post('/billing',userControllers.logBilling)

module.exports = router;