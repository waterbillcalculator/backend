const User = require('../models/User');

const fs = require('fs');

//thermalPrinter
const ThermalPrinter = require("../node_modules/node-thermal-printer").printer;
const PrinterTypes = require("../node_modules/node-thermal-printer").types;




module.exports.addUser = (req,res) => {

    let newUser = new User({

        accountNo: req.body.accountNo,
		fullName : req.body.fullName,
        address : req.body.address,
		meterNo : req.body.meterNo,
		meterBrand : req.body.meterBrand,
		previousReading : req.body.previousReading,
		currentReading : req.body.currentReading,
        isSenior : req.body.isSenior

	})

	newUser.save()
	.then(user => res.send(user))
	.catch(error => res.send(error))


	

}

module.exports.getUserDetails = (req,res) => {

	User.find({accountNo: req.body.accountNo})
	.then(result => {
		const [user] = result

		return res.send(user)
	})
	.catch(error => res.send(error))
}

module.exports.updateUser = (req,res) => {

	let updates = {
		accountNo: req.body.accountNo,
		fullName : req.body.fullName,
        address : req.body.address,
		meterNo : req.body.meterNo,
		meterBrand : req.body.meterBrand,
		previousReading : req.body.previousReading,
		currentReading : req.body.currentReading,
        isSenior : req.body.isSenior
	}

	User.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.logBilling = (req, res) => {

	const readingType = ""
	const meterNo = ""
	const meterBrand = ""
	const previousReading = ""
	const currentReading = ""
	const findings = ""
	const findingsCode = ""



	fs.appendFile("./db/test", `\n${req.body.accountNo};${readingType};${meterNo} ${meterBrand};${previousReading};${currentReading};${findings};${findingsCode};;${req.body.waterConsumption};${req.body.waterBill}`, function(err) {
		if(err) {
			return res.send(err);
		}
		res.send({"message":"file was saved"});
	}); 
	
}
