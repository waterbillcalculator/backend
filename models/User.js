const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

    accountNo: {
        type: String,
        required: [true, "Account Number is required"]
    },
	fullName: {
		type: String,
		required: [true, "Full Name is required."]
	},
	address: {
		type: String,
		required: [true, "Address is required."]
	},
	readingType: {
		type: String,
		default: "M"
	},
	meterNo: {
		type: String,
		required: [true, "Meter No. is required"]
	},
	meterBrand: {
		type: String,
		required: [true, "Meter brand is required"]
	},
	previousReading: {
		type: Number
	},
	currentReading: {
		type: Number
	},
	isSeniorCitizen: {
		type: Boolean,
		required: [true, "isSeniorCitizen is required"]
	}

})

module.exports = mongoose.model('User', userSchema)