const express = require('express');
const app = express();
const mongoose = require('mongoose');
const port = process.env.PORT || 4000;
const cors = require("cors");


mongoose.connect('mongodb+srv://angzang:angelo@cluster0.j7r8u.mongodb.net/waterDistrict?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let db = mongoose.connection;
db.on('error',console.error.bind(console, "Connection Error"));
db.once('open',() => console.log("Connected to MongoDB"));


app.use(cors())
app.use(express.json());

const userRoutes = require('./routes/userRoutes')
app.use('/', userRoutes);


app.listen(port, () => console.log(`Server is running at port ${port}`));
